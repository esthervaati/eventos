from google.appengine.api import users
from google.appengine.ext import ndb

import jinja2
import webapp2
import models

class Owner(ndb.Model):
    name = ndb.StringProperty(indexed=False)
    email = ndb.StringProperty(indexed=False)


class Event(ndb.Model):
    owner = ndb.StructuredProperty(Owner)
    name = ndb.StringProperty(indexed=False)
    description = ndb.StringProperty(indexed=False)
    location = ndb.StringProperty(indexed=False)
    date_added= ndb.DateTimeProperty(auto_now_add=True)
