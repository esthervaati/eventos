import os
import urllib

from google.appengine.api import users
from google.appengine.ext import ndb

import jinja2
import webapp2

import models

class Owner(ndb.Model):
    name = ndb.StringProperty(indexed=False)
    email = ndb.StringProperty(indexed=False)


class Event(ndb.Model):
    owner = ndb.StructuredProperty(Owner)
    name = ndb.StringProperty(indexed=False)
    description = ndb.StringProperty(indexed=False)
    location = ndb.StringProperty(indexed=False)
    date_added= ndb.DateTimeProperty(auto_now_add=True)




JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

class AddEvent(webapp2.RequestHandler):

    def post(self):
        event = Event()

        if users.get_current_user():
            event.owner = Owner(
                    name=users.get_current_user().user_id(),
                    email=users.get_current_user().email())

        event.description = self.request.get('description')
        event.name = self.request.get('name')
        event.location = self.request.get('location')
        event.put()
        self.redirect('/?')

class MainPage(webapp2.RequestHandler):

    def get(self):
       
        events = Event.query().order(-Event.date_added).fetch(12)
        # print "events"

        user = users.get_current_user()
        login_url = users.create_login_url(self.request.path)
        logout_url = users.create_logout_url(self.request.path)
    
        context = {
            'user': user,
            'events': events,
            'login_url': login_url,
            'logout_url': logout_url,
        }

        template = JINJA_ENVIRONMENT.get_template('home.html')
        self.response.write(template.render(context))

class EventBook(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        login_url = users.create_login_url(self.request.path)
        logout_url = users.create_logout_url(self.request.path)
    
        context = {
            'user': user,
            'login_url': login_url,
            'logout_url': logout_url,
        }
        template = JINJA_ENVIRONMENT.get_template('post_events.html')
        self.response.write(template.render(context))

app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/post_events', EventBook),
    ('/add', AddEvent),

], debug=True)

